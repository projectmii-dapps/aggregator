import "./style.css";
let events = require('events');
let PromisifyEmitter = require('promisify-event-emitter');


if (process.env.NODE_ENV !== 'production') {
	console.log('DEVELOPMENT MODE');
}

let request = require('request');
let crypto = require('crypto');
let StellarSdk = require('stellar-sdk');

const horizonTestnet = 'https://horizon-testnet.stellar.org';
const horizonTestnetFriendbot = 'https://horizon-testnet.stellar.org/friendbot';
let server = new StellarSdk.Server(horizonTestnet);


const pub = 'GAAJGBMOYYO7CCJDYR7O53ZLDTLEJBOSYEB4EMSZCSBBB45KLGL73ORQ';
const priv = 'SD3GCGE57LPAIQGLZUERSJ6MB274JEWQFR6WRPLDMPLD4PRC2NW2AIFY';


const MIN = '0.0000001';
const TX_CHUNK_SIZE = 200;

let txListener;

window.ledger = {
	start: function() {
		StellarSdk.Network.useTestNetwork();
		// this.searchTransactions();
	},

	setTxListener: function(onTx) {
		txListener = server.transactions()
		.forAccount(pub)
		.cursor('now').stream({
			onmessage: function(message) {
				console.log(message);
			}
		})
	},

	filterInputTxs: function(txs) {
		return txs.filter(tx => tx.source_account != pub);
	},

	filterOutputTxs: function(txs) {
		return txs.filter(tx => tx.source_account == pub);
	},

	filterDataTxs: function(txs) {
		return txs.filter(tx => tx.memo_type == 'hash');
	},

	submitMemoHashTx: async function(hash, receiver, callback) {
		let issuerPair = StellarSdk.Keypair.fromSecret(priv);

		let transaction;
		await server.loadAccount(pub).then(function(account) {
			transaction = new StellarSdk.TransactionBuilder(account)
			.addOperation(StellarSdk.Operation.payment({
				destination: receiver,
				asset: StellarSdk.Asset.native(),
				amount: MIN,
			}))
			.addMemo(StellarSdk.Memo.hash(hash))
			.build();
			transaction.sign(issuerPair);

			console.log('xdr: ', transaction.toEnvelope().toXDR('base64'));

			return account;
		})
		.catch(function(e) {
			console.error(e);
		});

		return server.submitTransaction(transaction)
		.then(function(transactionResult) {
			if (typeof(callback) == 'function') {
				callback(false);
			}
			else {
				console.log(typeof(callback), callback);
			}
			console.log(JSON.stringify(transactionResult, null, 2));
			console.log('\nSuccess! View the transaction at: ');
			console.log(transactionResult._links.transaction.href);
		})
		.catch(function(err) {
			console.log('An error has occured:');
			console.log(err);
		});
	},

	submitTxPipe: async function(pipe, onConfirmation, account) {
		let self = this;
		let issuerPair = StellarSdk.Keypair.fromSecret(priv);

		server.loadAccount(pub).then(function(pub) {
			let transaction = new StellarSdk.TransactionBuilder(pub)
			.addOperation(StellarSdk.Operation.payment({
				destination: account,
				asset: StellarSdk.Asset.native(),
				amount: MIN,
			}))
			.addMemo(StellarSdk.Memo.hash(pipe.pop()))
			.build();
			transaction.sign(issuerPair);

			console.log('xdr: ', transaction.toEnvelope().toXDR('base64'));

			server.submitTransaction(transaction)
			.then(function(transactionResult) {
				if (typeof(onConfirmation) == 'function') {
					onConfirmation(transactionResult);
				}
				else {
					console.log(typeof(onConfirmation), onConfirmation);
				}

				if (pipe.length > 0) {
					self.submitTxPipe(pipe, onConfirmation);
				}

				console.log(JSON.stringify(transactionResult, null, 2));
				console.log('\nSuccess! View the transaction at: ');
				console.log(transactionResult._links.transaction.href);

				return transactionResult;
			})
			.catch(function(err) {
				console.log('An error has occured:');
				console.log(err);
			});

			return pub;
		})
		.catch(function(e) {
			console.error(e);
		});
	},

	getTxs: function(account) {
		return server.transactions().forAccount(account)
		.limit(TX_CHUNK_SIZE).order('desc').call()
		.then(page => { return page; });
	},

	nextPage: function(txs, page, eventEmitter) {
		txs.push(...page.records);
		if (page.records.length == TX_CHUNK_SIZE) {
			page.next().then(page => this.nextPage(txs, page, eventEmitter));
		}
		else { eventEmitter.emit('done'); }
	},

	searchTransactions: function(account, txs) {
		if (typeof(txs) == 'undefined') txs = [];
		let eventEmitter = new events.EventEmitter();

		server.transactions().forAccount(account)
		.limit(TX_CHUNK_SIZE).order('desc').call()
		.then(page => this.nextPage(txs, page, eventEmitter));

		return PromisifyEmitter.on(eventEmitter, 'done');
	},

	getTransactions: async function(account) {
		let txHistory = [];
		await server.transactions().forAccount(account).call()
		.then(async function(page) {
			console.log('first page', page)
			page.prev().then(content => console.log('prev', content))
			txHistory.push(...page.records);
			while (page.records.length > 0) {
				page = await page.next();
				txHistory.push(...page.records);
			}
		})
		.catch(function(err) { console.log(err); });

		return txHistory;
	}
}

window.addEventListener('load', function() {
	ledger.start();
});
